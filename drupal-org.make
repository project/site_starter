; Drupal.org release file.
core = 7.x
api = 2

; Contributed modules
projects[admin_menu][subdir] = contrib
projects[admin_menu][version] = 3.0-rc4

projects[backup_migrate][subdir] = contrib
projects[backup_migrate][version] = 2.8

projects[browserclass][subdir] = contrib
projects[browserclass][version] = 1.7

projects[ctools][subdir] = contrib
projects[ctools][version] = 1.5

projects[diff][subdir] = contrib
projects[diff][version] = 3.2

projects[entity][subdir] = contrib
projects[entity][version] = 1.5

projects[features][subdir] = contrib
projects[features][version] = 2.2

projects[field_group][subdir] = contrib
projects[field_group][version] = 1.4

projects[fpa][subdir] = contrib
projects[fpa][version] = 2.6

projects[globalredirect][subdir] = contrib
projects[globalredirect][version] = 1.5

projects[google_analytics][subdir] = contrib
projects[google_analytics][version] = 1.4

projects[imce][subdir] = contrib
projects[imce][version] = 1.9

projects[imce_mkdir][subdir] = contrib
projects[imce_mkdir][version] = 1.0

projects[imce_wysiwyg][subdir] = contrib
projects[imce_wysiwyg][version] = 1.0

projects[libraries][subdir] = contrib
projects[libraries][version] = 2.2

projects[metatag][subdir] = contrib
projects[metatag][version] = 1.4

projects[module_filter][subdir] = contrib
projects[module_filter][version] = 1.8

projects[pathauto][subdir] = contrib
projects[pathauto][version] = 1.2

projects[strongarm][subdir] = shield
projects[strongarm][version] = 1.2

projects[strongarm][subdir] = contrib
projects[strongarm][version] = 2.0

projects[token][subdir] = contrib
projects[token][version] = 1.5

projects[views][subdir] = contrib
projects[views][version] = 3.8

projects[wysiwyg][subdir] = contrib
projects[wysiwyg][version] = 2.2

; Libraries
libraries[ckeditor][download][type] = get
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.2/ckeditor_3.6.2.tar.gz"
libraries[ckeditor][destination] = libraries
libraries[ckeditor][directory_name] = ckeditor
